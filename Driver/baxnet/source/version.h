/*
  version.h

  (C) 2018 Henryk Richter <henryk.richter@gmx.net>

  version and date handling
*/
#ifndef _INC_VERSION_H
#define _INC_VERSION_H

#define DEVICEVERSION  2
#define DEVICEREVISION 7
#define DEVICEEXTRA
/* #define DEVICEEXTRA Beta */
#define DEVICEDATE    11.02.20

#endif
