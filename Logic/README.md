Logic Files for Zorro-LAN-IDE-CP
================================

What's what?
------------

For the v0.2 up to the latest v0.3, the Xilinx firmware can be found in
the subdirectory ``LAN_IDE_V2``.

The most recent firmware files are:
```
LAN_IDE_CP.jed          - for Zorro III
LAN_IDE_CP-z2v2.jed     - for Zorro II (careful: The IDE off jumper is reversed in functionality)
```

Program and verify with ``xc3sprog`` and "Xilinx USB Platform Cable II":
```bash
xc3sprog -c xpc <JED-File>
```

The most recent flash ROM for the IDE autoboot part can be found in the
subdirectory ``Oktapussy``.

Description of the files:
```
Oktapussy_ROM.bin       - Original 64 kB ROM file for AT Bus 2008 clone
Oktapussy_ROM_128k.bin  - Two concatenated copies to fill an AM29F010B
```

Flash with "Minipro" programmer:
```bash
minipro -p "AM29F010B@PLCC32" -w Oktapussy_ROM_128k.bin
```
